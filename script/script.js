// Завдання
// Реалізувати функцію підсвічування клавіш.Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір.При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною.Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір.Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.



let activeBtn;
const keyBoard = {
  Enter: 'Enter',
  s: 's',
  e: 'e',
  o: 'o',
  n: 'n',
  l: 'l',
  z: 'z',
};


const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
  if (keyBoard[`${event.key}`]) {
    buttons.forEach(function (item) {
      item.classList.remove('pressed-btn')
    });
    document.querySelector(`[data="${event.key}"]`).classList.add('pressed-btn');
    activeBtn = document.querySelector(`[data="${event.key}"]`);
  }
});








